---
layout: 2020/post
section: proposals
category: workshops
title: NAME OF THE WORKSHOP
---

Brief introduction and motivation of the same.

## Objectives that will be covered in the workshop

Description of the objectives to be covered during the development of the workshop

## Target audience

Who should attend?

## Prerequisites for attendees

Description of minimum knowledge required, as well as hardware/software to be carried by the attendees.

## Speaker(s)

Who is going to organize the workshop? What do you/they do? (brief bio about yourself) What workshops have you organized before?

### Contact(s)

-   Name: contact

For "Name", use the full name. For "contact", use an email address (format "user @ domain"), or the GitLab user name (format "user @ GitLab). In any case, have into account that these addresses will be used for contacting you, so you better check them frequently ;-)

## Prerequisites for the organization

Describe the necessities of the workshop, such as a room with computers, the software,etc. The organization does not guarantee that the needs of the proposed workshops can be met.

## Time

Indicate the duration of the workshop. For example, from two hours to a whole day.

## Day

Indicate if you prefer to carry out the workshop on the first or second day.

## Observations

Any other relevant observations.

## Conditions

-   [ ]  I agree to follow the [code of conduct](https://eslib.re/conducta/) and request this acceptance from the attendees and speakers.
-   [ ]  At least one person among those proposing will be present on the day scheduled for the _workshop_.
-   [ ]  I agree to coordinate with the organization of esLibre.
