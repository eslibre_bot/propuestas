---
layout: 2020/post
section: proposals
category: workshops
title: NOMBRE DEL TALLER
---

Pequeña introducción y motivación del mismo.

## Objetivos a cubrir en el taller

Descripción de los objetivos a cubrir durante el desarrollo del taller

## Público objetivo

¿A quién va dirigida?

## Ponente(s)

¿Quién o quienes van a llevar el taller? ¿Qué hacen? ¿Qué talleres han
llevado antes?

### Contacto(s)

-   Nombre: contacto

Para "Nombre", utliza el nombre completo. Para "contacto", utiliza una dirección de correo (formato "usuario @ dominio"), o el nombre de usuario en GitLab (formato "usuario @ GitLab"). En cualquier caso, ten en cuenta que estas direcciones se usarán para entrar en contacto contigo, así que mejor si las consultas frecuentemente ;-)

## Prerrequisitos para los asistentes

Descripción de conocimientos mínimos necesarios, así como hardware/software que deban llevar los asistentes.

## Prerrequisitos para la organización

Describir las necesidades del taller, como sala con ordenadores, software instalado, etc. La organización no garantiza que se puedan satisfacer las necesidades de los talleres propuestos.

## Tiempo

Indicar la duración del taller. Por ejemplo, desde dos horas hasta un día entero.

## Día

Indicar si se prefiere realizar el taller el primer día o el segundo.

## Comentarios

Cualquier otro comentario relevante.

## Condiciones

-   [ ]  Acepto seguir el [código de conducta](https://eslib.re/conducta/).
-   [ ]  Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
-   [ ]  Acepto coordinarme con la organización de esLibre.
