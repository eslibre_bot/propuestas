---
layout: 2020/post
section: proposals
category: talks
title: GeoChicas&#58 Las calles de las mujeres
---

GeoChicas es un grupo dentro de la comunidad de OpenStreetMap que aporta una visión feminista a la recolección y clasificación de los datos. Uno de sus proyectos más famosos es "Las Calles de las Mujeres", una iniciativa para comparar qué porcentaje de calles llevan nombre de mujer.

## Formato de la propuesta

-   [ ]  Charla (25 minutos)
-   [x]  Charla relámpago (10 minutos)

## Descripción

GeochicasOSM es un grupo de mujeres que realizan mapeo en OpenStreetMap y trabajan para cerrar la brecha de género. El grupo fue creado en la Conferencia ‘SOTM LATAM 2016’, en São Paulo (Brasil) y actualmente hay usuarias en al menos 20 países de 3 continentes diferentes, entre ellos España.

‘Las calles de las mujeres’ es un proyecto colaborativo para producir un mapa a partir de las calles que cuenten con el nombre de alguna mujer. Busca enlazar y generar contenidos en OSM y Wikipedia sobre mujeres destacadas y así visibilizar la brecha de género que existe históricamente dentro de las ciudades.

## Público objetivo

Todos los públicos. Especialmente aquellos que trabajan con datos.

## Ponente(s)

-   **Isabel Vigil**: Ingeniera Técnica Forestal reconvertida a informática. Haciendo mapas con JS, OpenLayers y MapeaJS. Organizadora de PingAProgramadoras y Geoinquiet@sSVQ.

-   **María Arias de Reyna**: Feminista, Open Source Advocate, RedHatter, GeoInquieta, Crazy of the Pussy y Social Justice Sorceress.

### Contacto(s)

-   **María Arias de Reyna**: delawen at gmail dot com

## Comentarios

Ninguno.

## Condiciones

-   [x]  Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
-   [x]  Al menos una persona entre los que la proponen estará presente el día programado para la charla.
